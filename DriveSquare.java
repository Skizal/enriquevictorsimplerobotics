package finalProject;

import lejos.hardware.Button;
import lejos.hardware.Sound;
import lejos.hardware.motor.Motor;
import lejos.hardware.motor.UnregulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.Color;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

public class FinalProject {

	public static void main(String[] args) {
		System.out.println("Final Project");
		System.out.println("Press any key to start");
		Sound.beepSequenceUp(); //Make sound when ready
		Button.waitForAnyPress();

		//EV3ColorSensor colSensorLeft = new EV3ColorSensor(SensorPort.S1);
		EV3UltrasonicSensor distSensor = new EV3UltrasonicSensor(SensorPort.S4);
		EV3ColorSensor colSensorRight = new EV3ColorSensor(SensorPort.S3);
		
		int goalLineColor = Color.WHITE;
		int groundColorRight = colSensorRight.getColorID();
		//int groundColorLeft = colSensorLeft.getColorID();
		
		
		//Change for Regulated motor
		UnregulatedMotor motorB = new UnregulatedMotor(MotorPort.B);
		UnregulatedMotor motorC = new UnregulatedMotor(MotorPort.C);
		
		motorB.forward();
		motorC.forward();
		
		boolean stop = false;
		while (!stop) {		
			
			//Finish when press change the color of the ground (endline)
			if (/*(colSensorLeft.getColorID() == goalLineColor) ||*/ (colSensorRight.getColorID() == goalLineColor) || (Button.readButtons() == Button.ID_ENTER)) {
				stop = true;
			} else if (/*(groundColorLeft != colSensorLeft.getColorID()) ||*/ (groundColorRight != colSensorRight.getColorID())) {

				Sound.beep();
				motorB.backward();
				motorC.backward();
				Delay.msDelay(500);
				motorB.forward();
				motorB.setPower(10);
				motorC.setPower(10);
				Delay.msDelay(1000);
			} else if(distance(distSensor.getDistanceMode())) {
				Sound.twoBeeps();
				//Reduce speed and turn
				motorB.setPower(10);
				motorC.setPower(10);
				motorC.backward();
				motorB.forward();
			} else {
				//Go straight on
				motorB.forward();
				motorC.forward();
				motorB.setPower(20);
				motorC.setPower(20);
			}
		}
		motorB.stop();
		motorC.stop();
		motorB.close();
		motorC.close();
	}

	public static boolean distance(SampleProvider sp) {
		float [] sample = new float[sp.sampleSize()];
		sp.fetchSample(sample, 0);
		if (sample[0] > 0.25) {
			return false;
		}
		return true;
	}
}
