Code for Mobile Robotics by Enrique and Victor

## 1. List the following steps needed to run a program in the order needed to run a program.  Draw a line through those steps that are not needed.

(a) Open an existing file, or write a new one.  

(b) Press the dark grey button on the EV3.  

(c) Turn the wheels to make it move. -----------------------  

(d) Check that the robot is fully charged and switched on.  

(e) Press the �Start� button on the Program Debug window.  

(f) Open the robot menu and select Compile and Download. -----------------  
