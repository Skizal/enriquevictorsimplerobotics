
import lejos.hardware.*;
import lejos.hardware.motor.*;
import lejos.hardware.port.MotorPort;
import lejos.utility.Delay;

public class three360
{
	public static void main(String[] args)
	{
		System.out.println("Hello World!!");  
        Button.waitForAnyPress();   
        //code for 360 rotation 
        
        UnregulatedMotor motorA = new UnregulatedMotor(MotorPort.A);
        UnregulatedMotor motorB = new UnregulatedMotor(MotorPort.B);
		motorB.backward();
		motorA.forward();
		//make the turn at the same time to remain in the same place
		motorA.setPower(50);
		motorB.setPower(50);
		//adjust time to get a 360 degrees turn
		Delay.msDelay(2250);
		motorA.stop();
		motorB.stop();  
		motorA.close();
		motorB.close();
	}
	
}