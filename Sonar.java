package ev3;

import lejos.hardware.Button;
import lejos.hardware.motor.UnregulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

public class Sonar {

	public static void main(String[] args) {
		
		System.out.println("Sonar");
		Button.waitForAnyPress(); 
		
		EV3UltrasonicSensor sensorUS = new EV3UltrasonicSensor(SensorPort.S4);

		UnregulatedMotor motorC = new UnregulatedMotor(MotorPort.C);
		UnregulatedMotor motorB = new UnregulatedMotor(MotorPort.B);
		
		motorC.setPower(50);
		motorB.setPower(50);
		motorC.forward();
		motorB.forward();
		System.out.println("Forward before for");
		for( int i = 0; i < 6; ++i ) {
			System.out.println("Before while");
			while( !distance(sensorUS.getDistanceMode()) );
			System.out.println("After while");
			motorC.setPower(80);
			motorB.setPower(80);
			motorC.backward();
			motorB.backward();
			Delay.msDelay(1000);
			motorC.setPower(50);
			motorB.setPower(50);
			motorC.forward();
			motorB.backward();
			Delay.msDelay(750);
			motorC.forward();
			motorB.forward();
		}
		Delay.msDelay(2000);
		motorC.close();
		motorB.close();
		sensorUS.close();
		
	}
	
	public static boolean distance( SampleProvider sp ) {
		float [] sample = new float[sp.sampleSize()];
		sp.fetchSample(sample, 0);
		System.out.println(sample[0]);
		if ( sample[0] > 0.10 ) {
			return false;
		}
		else {
			return true;
		}
		
	}

}